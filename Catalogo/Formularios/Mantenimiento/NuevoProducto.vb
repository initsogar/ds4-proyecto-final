﻿Imports MySql.Data.MySqlClient
Imports System.Data
Imports System.Data.SqlClient
Imports System.IO
Imports System.Drawing
Imports System.Drawing.Imaging

Public Class NuevoProducto
    Dim ofdImagenProducto As New OpenFileDialog

    Private Sub btnAgregarProducto_Click(sender As Object, e As EventArgs) Handles btnAgregarProducto.Click
        Dim descripcion, imgName As String
        Dim cantidad, rop As Integer
        Dim precioUnitario As Single

        If (Validar.campoVacio({txtDescripcion.Text, txtCantidad.Text, txtROP.Text, txtPrecioUnitario.Text, txtRutaImagenProducto.Text})) Then
            MsgBox("Debe llenar todos los campos.")
            Exit Sub
        End If

        descripcion = txtDescripcion.Text
        cantidad = Integer.Parse(txtCantidad.Text)
        rop = Integer.Parse(txtROP.Text)
        precioUnitario = Math.Round(Convert.ToSingle(txtPrecioUnitario.Text), 2)
        imgName = ofdImagenProducto.FileName

        Try
            If (Productos.NuevoProducto(descripcion, cantidad, rop, precioUnitario, imgName)) Then
                MsgBox("¡Producto editado exitosamente!")
                Mantenimiento.RefreshData()
                Me.Hide()
                limpiarCampos()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub limpiarCampos()
        txtDescripcion.Clear()
        txtCantidad.Clear()
        txtPrecioUnitario.Clear()
        txtROP.Clear()
        txtRutaImagenProducto.Clear()
    End Sub
    Private Sub txtCantidad_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtCantidad.KeyPress
        If Not (Validar.campoNumerico(e.KeyChar)) Then
            e.KeyChar = ""
        End If
    End Sub

    Private Sub txtROP_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtROP.KeyPress
        If Not (Validar.campoNumerico(e.KeyChar)) Then
            e.KeyChar = ""
        End If
    End Sub

    Private Sub txtPrecioUnitario_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtPrecioUnitario.KeyPress
        If Not (Validar.campoNumerico(e.KeyChar)) Then
            e.KeyChar = ""
        End If
    End Sub

    Private Sub btnExaminarImagen_Click(sender As Object, e As EventArgs) Handles btnExaminarImagen.Click
        ofdImagenProducto.Filter = "Picture Files (*)|*.bmp;*.gif;*.jpg;*.png"
        If ofdImagenProducto.ShowDialog = Windows.Forms.DialogResult.OK Then
            txtRutaImagenProducto.Text = ofdImagenProducto.FileName
        End If
    End Sub

    Private Sub NuevoProducto_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        Mantenimiento.lblEstado.Text = "Productos cargados"
    End Sub
End Class