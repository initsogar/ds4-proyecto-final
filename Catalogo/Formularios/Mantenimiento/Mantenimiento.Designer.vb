﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Mantenimiento
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Mantenimiento))
        Me.ProductosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DstProductos = New ProyectoInventario.dstProductos()
        Me.FileSystemWatcher1 = New System.IO.FileSystemWatcher()
        Me.ProductosTableAdapter = New ProyectoInventario.dstProductosTableAdapters.productosTableAdapter()
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip()
        Me.tsbNuevoProducto = New System.Windows.Forms.ToolStripButton()
        Me.tsbEditarProducto = New System.Windows.Forms.ToolStripButton()
        Me.tsbEliminarProducto = New System.Windows.Forms.ToolStripButton()
        Me.cbbFiltro = New System.Windows.Forms.ToolStripComboBox()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.lblEstado = New System.Windows.Forms.ToolStripStatusLabel()
        Me.PrecioDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.RopDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CantidadDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DescripcionDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.imagen = New System.Windows.Forms.DataGridViewImageColumn()
        Me.IdDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.dgwProductos = New System.Windows.Forms.DataGridView()
        CType(Me.ProductosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DstProductos, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.FileSystemWatcher1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.ToolStrip1.SuspendLayout()
        Me.StatusStrip1.SuspendLayout()
        CType(Me.dgwProductos, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'ProductosBindingSource
        '
        Me.ProductosBindingSource.DataMember = "productos"
        Me.ProductosBindingSource.DataSource = Me.DstProductos
        '
        'DstProductos
        '
        Me.DstProductos.DataSetName = "dstProductos"
        Me.DstProductos.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'FileSystemWatcher1
        '
        Me.FileSystemWatcher1.EnableRaisingEvents = True
        Me.FileSystemWatcher1.SynchronizingObject = Me
        '
        'ProductosTableAdapter
        '
        Me.ProductosTableAdapter.ClearBeforeFill = True
        '
        'ToolStrip1
        '
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsbNuevoProducto, Me.tsbEditarProducto, Me.tsbEliminarProducto, Me.cbbFiltro})
        Me.ToolStrip1.Location = New System.Drawing.Point(0, 0)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.Size = New System.Drawing.Size(544, 25)
        Me.ToolStrip1.TabIndex = 1
        Me.ToolStrip1.Text = "ToolStrip1"
        '
        'tsbNuevoProducto
        '
        Me.tsbNuevoProducto.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tsbNuevoProducto.Image = Global.ProyectoInventario.My.Resources.Resources._1405025219_add
        Me.tsbNuevoProducto.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsbNuevoProducto.Name = "tsbNuevoProducto"
        Me.tsbNuevoProducto.Size = New System.Drawing.Size(23, 22)
        Me.tsbNuevoProducto.Text = "Agregar nuevo producto"
        '
        'tsbEditarProducto
        '
        Me.tsbEditarProducto.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tsbEditarProducto.Image = Global.ProyectoInventario.My.Resources.Resources._1405057913_Modify
        Me.tsbEditarProducto.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsbEditarProducto.Name = "tsbEditarProducto"
        Me.tsbEditarProducto.Size = New System.Drawing.Size(23, 22)
        Me.tsbEditarProducto.Text = "Editar producto"
        '
        'tsbEliminarProducto
        '
        Me.tsbEliminarProducto.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tsbEliminarProducto.Image = Global.ProyectoInventario.My.Resources.Resources._1405048184_delete
        Me.tsbEliminarProducto.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsbEliminarProducto.Name = "tsbEliminarProducto"
        Me.tsbEliminarProducto.Size = New System.Drawing.Size(23, 22)
        Me.tsbEliminarProducto.Text = "Eliminar producto"
        '
        'cbbFiltro
        '
        Me.cbbFiltro.Items.AddRange(New Object() {"Todos", "Deben ordenarse", "Con existencia 0"})
        Me.cbbFiltro.Name = "cbbFiltro"
        Me.cbbFiltro.Size = New System.Drawing.Size(121, 25)
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.lblEstado})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 343)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(544, 22)
        Me.StatusStrip1.SizingGrip = False
        Me.StatusStrip1.TabIndex = 2
        Me.StatusStrip1.Text = "stp"
        '
        'lblEstado
        '
        Me.lblEstado.Name = "lblEstado"
        Me.lblEstado.Size = New System.Drawing.Size(0, 17)
        '
        'PrecioDataGridViewTextBoxColumn
        '
        Me.PrecioDataGridViewTextBoxColumn.DataPropertyName = "precio"
        Me.PrecioDataGridViewTextBoxColumn.HeaderText = "Precio"
        Me.PrecioDataGridViewTextBoxColumn.Name = "PrecioDataGridViewTextBoxColumn"
        Me.PrecioDataGridViewTextBoxColumn.ReadOnly = True
        '
        'RopDataGridViewTextBoxColumn
        '
        Me.RopDataGridViewTextBoxColumn.DataPropertyName = "rop"
        Me.RopDataGridViewTextBoxColumn.HeaderText = "Punto de reorden"
        Me.RopDataGridViewTextBoxColumn.Name = "RopDataGridViewTextBoxColumn"
        Me.RopDataGridViewTextBoxColumn.ReadOnly = True
        Me.RopDataGridViewTextBoxColumn.Width = 120
        '
        'CantidadDataGridViewTextBoxColumn
        '
        Me.CantidadDataGridViewTextBoxColumn.DataPropertyName = "cantidad"
        Me.CantidadDataGridViewTextBoxColumn.HeaderText = "En existencia"
        Me.CantidadDataGridViewTextBoxColumn.Name = "CantidadDataGridViewTextBoxColumn"
        Me.CantidadDataGridViewTextBoxColumn.ReadOnly = True
        '
        'DescripcionDataGridViewTextBoxColumn
        '
        Me.DescripcionDataGridViewTextBoxColumn.DataPropertyName = "descripcion"
        Me.DescripcionDataGridViewTextBoxColumn.HeaderText = "Descripción"
        Me.DescripcionDataGridViewTextBoxColumn.Name = "DescripcionDataGridViewTextBoxColumn"
        Me.DescripcionDataGridViewTextBoxColumn.ReadOnly = True
        '
        'imagen
        '
        Me.imagen.DataPropertyName = "imagen"
        Me.imagen.FillWeight = 50.0!
        Me.imagen.HeaderText = "Imagen"
        Me.imagen.ImageLayout = System.Windows.Forms.DataGridViewImageCellLayout.Stretch
        Me.imagen.Name = "imagen"
        Me.imagen.ReadOnly = True
        Me.imagen.Width = 50
        '
        'IdDataGridViewTextBoxColumn
        '
        Me.IdDataGridViewTextBoxColumn.DataPropertyName = "id"
        Me.IdDataGridViewTextBoxColumn.HeaderText = "ID"
        Me.IdDataGridViewTextBoxColumn.Name = "IdDataGridViewTextBoxColumn"
        Me.IdDataGridViewTextBoxColumn.ReadOnly = True
        Me.IdDataGridViewTextBoxColumn.Width = 30
        '
        'dgwProductos
        '
        Me.dgwProductos.AllowUserToAddRows = False
        Me.dgwProductos.AllowUserToDeleteRows = False
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.dgwProductos.AlternatingRowsDefaultCellStyle = DataGridViewCellStyle1
        Me.dgwProductos.AutoGenerateColumns = False
        Me.dgwProductos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgwProductos.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.IdDataGridViewTextBoxColumn, Me.imagen, Me.DescripcionDataGridViewTextBoxColumn, Me.CantidadDataGridViewTextBoxColumn, Me.RopDataGridViewTextBoxColumn, Me.PrecioDataGridViewTextBoxColumn})
        Me.dgwProductos.DataSource = Me.ProductosBindingSource
        Me.dgwProductos.EditMode = System.Windows.Forms.DataGridViewEditMode.EditOnF2
        Me.dgwProductos.Location = New System.Drawing.Point(12, 28)
        Me.dgwProductos.MultiSelect = False
        Me.dgwProductos.Name = "dgwProductos"
        Me.dgwProductos.ReadOnly = True
        Me.dgwProductos.RowHeadersVisible = False
        Me.dgwProductos.RowTemplate.DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        Me.dgwProductos.RowTemplate.DefaultCellStyle.Padding = New System.Windows.Forms.Padding(5)
        Me.dgwProductos.RowTemplate.Height = 50
        Me.dgwProductos.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgwProductos.Size = New System.Drawing.Size(520, 311)
        Me.dgwProductos.TabIndex = 0
        '
        'Mantenimiento
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(544, 365)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.ToolStrip1)
        Me.Controls.Add(Me.dgwProductos)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "Mantenimiento"
        Me.Text = "Mantenimiento de Inventario"
        CType(Me.ProductosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DstProductos, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.FileSystemWatcher1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        CType(Me.dgwProductos, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents FileSystemWatcher1 As System.IO.FileSystemWatcher
    Friend WithEvents DstProductos As ProyectoInventario.dstProductos
    Friend WithEvents ProductosBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents ProductosTableAdapter As ProyectoInventario.dstProductosTableAdapters.productosTableAdapter
    Friend WithEvents ToolStrip1 As System.Windows.Forms.ToolStrip
    Friend WithEvents tsbNuevoProducto As System.Windows.Forms.ToolStripButton
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents tsbEditarProducto As System.Windows.Forms.ToolStripButton
    Friend WithEvents lblEstado As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents tsbEliminarProducto As System.Windows.Forms.ToolStripButton
    Friend WithEvents dgwProductos As System.Windows.Forms.DataGridView
    Friend WithEvents IdDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents imagen As System.Windows.Forms.DataGridViewImageColumn
    Friend WithEvents DescripcionDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents CantidadDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents RopDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PrecioDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cbbFiltro As System.Windows.Forms.ToolStripComboBox

End Class
