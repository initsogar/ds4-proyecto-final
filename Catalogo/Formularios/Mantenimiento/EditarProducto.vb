﻿Public Class EditarProducto

    Private Sub btnEditarProducto_Click(sender As Object, e As EventArgs) Handles btnEditarProducto.Click
        Dim descripcion As String
        Dim cantidad, rop, id As Integer
        Dim precioUnitario As Single

        If (Validar.campoVacio({txtDescripcion.Text, txtCantidad.Text, txtROP.Text, txtPrecioUnitario.Text})) Then
            MsgBox("Debe llenar todos los campos.")
            Exit Sub
        End If

        id = Integer.Parse(txtId.Text)
        descripcion = txtDescripcion.Text
        cantidad = Integer.Parse(txtCantidad.Text)
        rop = Integer.Parse(txtROP.Text)
        precioUnitario = Math.Round(Convert.ToSingle(txtPrecioUnitario.Text), 2)

        Try
            If (Productos.EditarProducto(id, descripcion, cantidad, rop, precioUnitario)) Then
                MsgBox("¡Producto editado exitosamente!")
                Mantenimiento.RefreshData()
                Me.Hide()
                limpiarCampos()
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub limpiarCampos()
        txtDescripcion.Clear()
        txtCantidad.Clear()
        txtPrecioUnitario.Clear()
        txtROP.Clear()
    End Sub
    Private Sub txtCantidad_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtCantidad.KeyPress
        If Not (Validar.campoNumerico(e.KeyChar)) Then
            e.KeyChar = ""
        End If
    End Sub

    Private Sub txtROP_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtROP.KeyPress
        If Not (Validar.campoNumerico(e.KeyChar)) Then
            e.KeyChar = ""
        End If
    End Sub

    Private Sub txtPrecioUnitario_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtPrecioUnitario.KeyPress
        If Not (Validar.campoNumerico(e.KeyChar)) Then
            e.KeyChar = ""
        End If
    End Sub

    Private Sub EditarProducto_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        Mantenimiento.lblEstado.Text = "Productos cargados"
    End Sub
End Class