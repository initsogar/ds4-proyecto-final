﻿Imports MySql.Data.MySqlClient

Public Class Mantenimiento

    Private Sub Mantenimiento_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        Dashboard.Show()
    End Sub

    Private Sub Mantenimiento_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        cbbFiltro.SelectedIndex = 0
        lblEstado.Text = "Cargando productos..."
        'TODO: This line of code loads data into the 'DstProductos.productos' table. You can move, or remove it, as needed.
        'Me.ProductosTableAdapter.Fill(Me.DstProductos.productos)

        lblEstado.Text = "Productos cargados"
    End Sub

    Public Sub RefreshData()
        cbbFiltro.SelectedIndex = 0
        lblEstado.Text = "Actualizando productos..."
        Me.ProductosTableAdapter.Fill(Me.DstProductos.productos)
        Me.dgwProductos.DataSource = Me.DstProductos.productos
        Me.dgwProductos.Refresh()
        lblEstado.Text = "Productos cargados"
    End Sub

    Private Sub dgwProductos_CellMouseDoubleClick(sender As Object, e As DataGridViewCellMouseEventArgs) Handles dgwProductos.CellMouseDoubleClick
        If e.RowIndex >= 0 AndAlso e.ColumnIndex >= 0 Then
            Dim selectedRow = dgwProductos.CurrentRow
            _editarProducto(selectedRow.Cells(0).Value)
        End If
    End Sub

    Private Sub tsbNuevoProducto_Click(sender As Object, e As EventArgs) Handles tsbNuevoProducto.Click
        lblEstado.Text = "Agregando nuevo producto..."
        NuevoProducto.Show()
    End Sub

    Private Sub tsbEditarProducto_Click(sender As Object, e As EventArgs) Handles tsbEditarProducto.Click
        Dim selectedRow = dgwProductos.CurrentRow
        _editarProducto(selectedRow.Cells(0).Value)
    End Sub

    Private Sub _editarProducto(ByVal id As Integer)
        lblEstado.Text = "Editando producto " & id & "..."

        Dim prods As Dictionary(Of String, String) = Productos.TraerProducto(id)

        EditarProducto.txtId.Text = prods("id")
        EditarProducto.txtDescripcion.Text = prods("descripcion")
        EditarProducto.txtCantidad.Text = prods("cantidad")
        EditarProducto.txtROP.Text = prods("rop")
        EditarProducto.txtPrecioUnitario.Text = prods("precio")

        EditarProducto.Show()
    End Sub

    Private Sub tsbEliminarProducto_Click(sender As Object, e As EventArgs) Handles tsbEliminarProducto.Click
        If MsgBox("¿En serio desea eliminar este producto?", MsgBoxStyle.YesNo, "NetStore Panamá") = MsgBoxResult.Yes Then
            Dim selectedRow = dgwProductos.CurrentRow
            Dim id = selectedRow.Cells(0).Value

            lblEstado.Text = "Eliminando producto " & id & "..."

            Productos.EliminarProducto(selectedRow.Cells(0).Value)
            MsgBox("Producto eliminado exitosamente.")
            RefreshData()
        End If
    End Sub

    Private Sub cbbFiltro_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbbFiltro.SelectedIndexChanged
        Dim query As String

        Select Case cbbFiltro.SelectedIndex
            Case 0 'Todos
                query = "SELECT * FROM productos where true"
            Case 1 'Faltantes
                query = "SELECT * FROM productos where cantidad <= rop"
            Case Else 'Existencia 0
                query = "SELECT * FROM productos where cantidad = 0"
        End Select

        DBHandler.GetInstance().openConnection()
        Dim connection As MySqlConnection = DBHandler.GetInstance().getConnection()

        Dim ds As DataSet = New DataSet
        Dim adapter = New MySqlDataAdapter(query, connection)
        adapter.Fill(ds, "productos")

        dgwProductos.DataSource = ds
        dgwProductos.DataMember = "productos"
        dgwProductos.Refresh()
        DBHandler.GetInstance().closeConnection()
    End Sub
End Class
