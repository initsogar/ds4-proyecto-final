﻿Public Class NuevaOrden
    Dim productoActual As Dictionary(Of String, String)

    Private Sub txtCodigo_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtCodigo.KeyPress
        If Not (Validar.campoNumerico(e.KeyChar)) Then
            e.KeyChar = ""
        End If
    End Sub

    Private Sub btnBuscarProducto_Click(sender As Object, e As EventArgs) Handles btnBuscarProducto.Click
        If Validar.campoVacio(txtCodigo.Text) Then
            MsgBox("Debe introducir un código de producto.", MsgBoxStyle.ApplicationModal, "NetStore Panamá")
            btnVenta.Enabled = False
            limpiarCampos()
        Else
            Dim codigo = Integer.Parse(txtCodigo.Text)

            If (Productos.ExisteProducto(codigo)) Then
                productoActual = Productos.TraerProducto(Integer.Parse(txtCodigo.Text))

                If (Integer.Parse(productoActual("cantidad")) = 0) Then
                    MsgBox("Este producto no está disponible para venta en estos momentos.", MsgBoxStyle.ApplicationModal, "NetStore Panamá")
                    btnVenta.Enabled = False
                    limpiarCampos()
                Else
                    btnVenta.Enabled = True
                    txtDescripcion.Text = productoActual("descripcion")
                    txtCantidad.Text = productoActual("cantidad")
                    txtPrecioUnitario.Text = productoActual("precio")
                End If

            Else
                MsgBox("No existe ningún producto con este código. Por favor, verifique de nuevo.", MsgBoxStyle.ApplicationModal, "NetStore Panamá")
                btnVenta.Enabled = False
                limpiarCampos()
            End If
        End If
    End Sub

    Private Sub limpiarCampos()
        txtCantidadDeseada.Text = ""
        txtCantidad.Text = ""
        txtCodigo.Text = ""
        txtDescripcion.Text = ""
        txtPrecioUnitario.Text = ""
    End Sub

    Private Sub NuevaOrden_FormClosing(sender As Object, e As FormClosingEventArgs) Handles Me.FormClosing
        Dashboard.Show()
    End Sub

    Private Sub txtCantidadDeseada_KeyPress(sender As Object, e As KeyPressEventArgs) Handles txtCantidadDeseada.KeyPress
        If Not (Validar.campoNumerico(e.KeyChar)) Then
            e.KeyChar = ""
        End If
    End Sub

    Private Sub btnVenta_Click(sender As Object, e As EventArgs) Handles btnVenta.Click
        If (Validar.campoVacio(txtCantidadDeseada.Text)) Then
            MsgBox("Debe escribir la cantidad deseada.", MsgBoxStyle.ApplicationModal, "NetStore Panamá")
        ElseIf (Integer.Parse(txtCantidadDeseada.Text) <= 0) Then
            MsgBox("Por favor, introduzca un número mayor que 0.", MsgBoxStyle.ApplicationModal, "NetStore Panamá")
        Else
            Dim cantidadDeseada = Integer.Parse(txtCantidadDeseada.Text)
            Dim cantidadActual = Integer.Parse(productoActual("cantidad"))

            If cantidadActual >= cantidadDeseada Then
                Productos.ActualizarProducto(productoActual("id"), cantidadActual - cantidadDeseada)
                Transaccion.lblDescripcion.Text = productoActual("descripcion")
                Transaccion.lblCostoTotal.Text = "B/. " & cantidadDeseada * productoActual("precio")
                Transaccion.lblCantidadPedido.Text = cantidadDeseada
                Transaccion.lblCantidadVendido.Text = cantidadDeseada
                Transaccion.lblFecha.Text = Date.Now.Day & "/" & Date.Now.Month & "/" & Date.Now.Year
                Transaccion.Show()
            Else
                Productos.ActualizarProducto(productoActual("id"), 0)
                Transaccion.lblDescripcion.Text = productoActual("descripcion")
                Transaccion.lblCostoTotal.Text = "B/. " & cantidadActual * productoActual("precio")
                Transaccion.lblCantidadPedido.Text = cantidadDeseada
                Transaccion.lblCantidadVendido.Text = cantidadActual
                Transaccion.lblFecha.Text = Date.Now.Day & "/" & Date.Now.Month & "/" & Date.Now.Year
                Transaccion.Show()
            End If

            limpiarCampos()
        End If
    End Sub
End Class