﻿Public Class Login

    Private Sub btnLogin_Click(sender As Object, e As EventArgs) Handles btnLogin.Click
        Dim user = txtUser.Text
        Dim password = txtPassword.Text

        If (Validar.campoVacio({user, password})) Then
            MsgBox("Debe llenar todos los campos")
        Else
            If user = "administrador" And password = "123" Then
                Me.Hide()
                Dashboard.Show()
            Else
                MsgBox("El usuario o la contraseña son incorrectas. Favor verificar.", MsgBoxStyle.ApplicationModal, "NetStore Panamá")
                txtPassword.Text = ""
                txtUser.Text = ""
            End If
        End If
    End Sub
End Class