﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Dashboard
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Dashboard))
        Me.lblMantenimiento = New System.Windows.Forms.Label()
        Me.lblNuevaOrden = New System.Windows.Forms.Label()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.btnMantenimiento = New System.Windows.Forms.Button()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblMantenimiento
        '
        Me.lblMantenimiento.AutoSize = True
        Me.lblMantenimiento.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMantenimiento.Location = New System.Drawing.Point(276, 211)
        Me.lblMantenimiento.Name = "lblMantenimiento"
        Me.lblMantenimiento.Size = New System.Drawing.Size(89, 13)
        Me.lblMantenimiento.TabIndex = 8
        Me.lblMantenimiento.Text = "Mantenimiento"
        '
        'lblNuevaOrden
        '
        Me.lblNuevaOrden.AutoSize = True
        Me.lblNuevaOrden.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNuevaOrden.Location = New System.Drawing.Point(94, 211)
        Me.lblNuevaOrden.Name = "lblNuevaOrden"
        Me.lblNuevaOrden.Size = New System.Drawing.Size(80, 13)
        Me.lblNuevaOrden.TabIndex = 10
        Me.lblNuevaOrden.Text = "Nueva orden"
        '
        'Button1
        '
        Me.Button1.BackgroundImage = Global.ProyectoInventario.My.Resources.Resources._1405069236_tests
        Me.Button1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.Button1.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.Button1.Location = New System.Drawing.Point(101, 144)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(64, 64)
        Me.Button1.TabIndex = 9
        Me.Button1.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.Button1.UseVisualStyleBackColor = True
        '
        'btnMantenimiento
        '
        Me.btnMantenimiento.BackgroundImage = Global.ProyectoInventario.My.Resources.Resources._1405069011_maintenance
        Me.btnMantenimiento.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.btnMantenimiento.ImageAlign = System.Drawing.ContentAlignment.TopCenter
        Me.btnMantenimiento.Location = New System.Drawing.Point(289, 144)
        Me.btnMantenimiento.Name = "btnMantenimiento"
        Me.btnMantenimiento.Size = New System.Drawing.Size(64, 64)
        Me.btnMantenimiento.TabIndex = 7
        Me.btnMantenimiento.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnMantenimiento.UseVisualStyleBackColor = True
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = Global.ProyectoInventario.My.Resources.Resources.netstore
        Me.PictureBox1.InitialImage = Global.ProyectoInventario.My.Resources.Resources.netstore
        Me.PictureBox1.Location = New System.Drawing.Point(101, 12)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(252, 100)
        Me.PictureBox1.TabIndex = 6
        Me.PictureBox1.TabStop = False
        '
        'Dashboard
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(468, 251)
        Me.Controls.Add(Me.lblNuevaOrden)
        Me.Controls.Add(Me.Button1)
        Me.Controls.Add(Me.lblMantenimiento)
        Me.Controls.Add(Me.btnMantenimiento)
        Me.Controls.Add(Me.PictureBox1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "Dashboard"
        Me.Text = "Dashboard"
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents btnMantenimiento As System.Windows.Forms.Button
    Friend WithEvents lblMantenimiento As System.Windows.Forms.Label
    Friend WithEvents lblNuevaOrden As System.Windows.Forms.Label
    Friend WithEvents Button1 As System.Windows.Forms.Button
End Class
