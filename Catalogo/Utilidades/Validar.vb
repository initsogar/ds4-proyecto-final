﻿Public Class Validar



    Public Shared Function campoNumerico(ByVal dato As String) As Boolean
        If (Not IsNumeric(dato)) And dato <> "," And dato <> ChrW(Keys.Back) And dato <> ChrW(Keys.Enter) Then
            Return False
        Else
            Return True
        End If
    End Function

    Public Shared Function campoVacio(ByVal campos() As String)
        Dim vacio As Boolean = False

        For Each campo As String In campos
            If campo = "" Then
                vacio = True
            Else
                vacio = False
            End If
        Next

        Return vacio
    End Function

    Public Shared Function campoVacio(ByVal campo As String)
        Return campo = ""
    End Function
End Class
