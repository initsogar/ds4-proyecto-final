﻿Imports MySql.Data.MySqlClient
Imports System.Data
Imports System.Data.SqlClient
Imports System.IO

Public Class Productos

    Public Shared Function NuevoProducto(ByVal descripcion, ByVal cantidad, ByVal rop, ByVal precioUnitario, ByVal image)
        Dim fInfo As New FileInfo(image)
        Dim len As Long = fInfo.Length

        DBHandler.GetInstance().openConnection()

        Dim cmd As MySqlCommand = New MySqlCommand()
        Using stream As New FileStream(image.Trim(), FileMode.Open)
            Dim imgData() As Byte = New Byte(Convert.ToInt32(len - 1)) {}
            stream.Read(imgData, 0, len)

            With cmd
                .Connection = DBHandler.GetInstance().getConnection()
                .CommandType = CommandType.Text
                .CommandText = "INSERT INTO productos (descripcion, cantidad, rop, precio, imagen) values (@descripcion,@cantidad,@rop,@precio,@imagen)"
                .Parameters.AddWithValue("descripcion", descripcion)
                .Parameters.AddWithValue("cantidad", cantidad)
                .Parameters.AddWithValue("rop", rop)
                .Parameters.AddWithValue("precio", precioUnitario)
                .Parameters.AddWithValue("imagen", imgData)
                .ExecuteNonQuery()
            End With
        End Using

        DBHandler.GetInstance().closeConnection()

        Return True
    End Function

    Public Shared Function EditarProducto(ByVal id, ByVal descripcion, ByVal cantidad, ByVal rop, ByVal precioUnitario)

        DBHandler.GetInstance().openConnection()

        Dim cmd As MySqlCommand = New MySqlCommand()

        With cmd
            .Connection = DBHandler.GetInstance().getConnection()
            .CommandType = CommandType.Text
            .CommandText = "UPDATE productos SET descripcion = @descripcion, cantidad = @cantidad, rop = @rop, precio = @precio WHERE id = @id"
            .Parameters.AddWithValue("descripcion", descripcion)
            .Parameters.AddWithValue("cantidad", cantidad)
            .Parameters.AddWithValue("rop", rop)
            .Parameters.AddWithValue("precio", precioUnitario)
            .Parameters.AddWithValue("id", id)
            .ExecuteNonQuery()
        End With

        DBHandler.GetInstance().closeConnection()

        Return True
    End Function

    Public Shared Function EliminarProducto(ByVal id)

        DBHandler.GetInstance().openConnection()

        Dim cmd As MySqlCommand = New MySqlCommand()

        With cmd
            .Connection = DBHandler.GetInstance().getConnection()
            .CommandType = CommandType.Text
            .CommandText = "DELETE FROM productos WHERE id = @id"
            .Parameters.AddWithValue("id", id)
            .ExecuteNonQuery()
        End With

        DBHandler.GetInstance().closeConnection()

        Return True
    End Function

    Public Shared Function ExisteProducto(ByVal id As Integer)
        DBHandler.GetInstance().openConnection()

        Dim cmd As MySqlCommand = New MySqlCommand()

        With cmd
            .Connection = DBHandler.GetInstance().getConnection()
            .CommandText = "SELECT * FROM productos WHERE id = @id"
            .CommandType = CommandType.Text
            .Parameters.AddWithValue("@id", id)
        End With

        Dim sqlReader As MySqlDataReader = cmd.ExecuteReader()

        If (sqlReader.HasRows) Then
            DBHandler.GetInstance().closeConnection()
            Return True
        Else
            DBHandler.GetInstance().closeConnection()
            Return False
        End If
    End Function

    Public Shared Function TraerProducto(ByVal id As Integer)
        Dim dict = New Dictionary(Of String, String)

        DBHandler.GetInstance().openConnection()

        Dim cmd As MySqlCommand = New MySqlCommand()

        With cmd
            .Connection = DBHandler.GetInstance().getConnection()
            .CommandText = "SELECT * FROM productos WHERE id = @id"
            .CommandType = CommandType.Text
            .Parameters.AddWithValue("@id", id)
        End With

        Dim sqlReader As MySqlDataReader = cmd.ExecuteReader()
        sqlReader.Read()

        dict.Add("id", sqlReader("id"))
        dict.Add("descripcion", sqlReader("descripcion"))
        dict.Add("cantidad", sqlReader("cantidad"))
        dict.Add("rop", sqlReader("rop"))
        dict.Add("precio", sqlReader("precio"))

        DBHandler.GetInstance().closeConnection()

        Return dict
    End Function


    Public Shared Function ActualizarProducto(ByVal id, ByVal cantidad)
        DBHandler.GetInstance().openConnection()

        Dim cmd As MySqlCommand = New MySqlCommand()

        With cmd
            .Connection = DBHandler.GetInstance().getConnection()
            .CommandType = CommandType.Text
            .CommandText = "UPDATE productos SET cantidad = @cantidad WHERE id = @id"
            .Parameters.AddWithValue("cantidad", cantidad)
            .Parameters.AddWithValue("id", id)
            .ExecuteNonQuery()
        End With

        DBHandler.GetInstance().closeConnection()

        Return True
    End Function

End Class
