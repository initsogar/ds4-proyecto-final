﻿Imports MySql.Data.MySqlClient

Public Class DBHandler
    Private connection As MySqlConnection
    Private connectionString As String
    Private Shared _instance As DBHandler = Nothing

    Private Sub New()
        'Remote
        'connectionString = "Server=192.241.160.141;Database=ds4;Uid=ds4;Pwd=prueba123;"

        'Local
        connectionString = "Server=localhost;Database=ds4;Uid=root;Pwd=root;"

        connection = New MySqlConnection(Me.connectionString)
    End Sub

    Public Sub openConnection()
        connection.Open()
    End Sub

    Public Sub closeConnection()
        connection.Close()
    End Sub

    Public Function getConnection()
        Return connection
    End Function

    Public Shared Function GetInstance() As DBHandler
        If IsNothing(_instance) Then
            _instance = New DBHandler()
        End If

        Return _instance
    End Function
End Class
